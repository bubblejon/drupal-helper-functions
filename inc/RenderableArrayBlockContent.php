<?php
	
	/**
	 *	Bulid a renderable array to be passed to a modules hook_theme.
	 *
	 *	@param string $theme The machine readable name of the theme being used.
	 *	@param array $themeVars Multidimensional array. Keys available: 'varName', 'nodeType' (both are required) and 'nodeStatus'. Alternatively 'sql' and 'sqlVars' used to overwrite the default query.
	 *	@param array $options Further options used in the renderable array such as '#attached' or '#markup' if '$themeVars' is empty.
	 *	
	 *	@return array Returns a renderable array that can be used as a blocks content.
	 *
	 *	@author @jonathandey
	 *	@copyright 2013-2014 Bubble Design & Marketing 
	 */
	function _collect_nodes($theme = '', $themeVars = array(), $options = array())
	{
		/* Set the renderable array */
		$renderable = array();

		/* Set the theme of the page, if provided */
		if($theme)
		{
			$renderable['#theme'] = $theme;
		}

		/* If there are variables attached to the theme lets run through them and attach data */
		if(count($themeVars))
		{
			foreach($themeVars as $v)
			{
				/* We need the name and type keys so we'll skip if they're not supplied */
				if((!array_key_exists('varName', $v) || !array_key_exists('nodeType', $v)) && (!array_key_exists('sql', $v) && !array_key_exists('sqlVars', $v)))
				{
					continue;
				}

				/* Node status will default to 1 (published) if not otherwise provided */
				if(!array_key_exists('nodeStatus', $v))
				{
					$v['nodeStatus'] = 1;
				}

				$nodes = array();

				/* If there isn't any SQL provided we'll just use this basic query */
				if(!array_key_exists('sql', $v))
				{
					$v['sql'] = 'SELECT {nid}
						FROM {node}
						WHERE type = :type
						AND status = :status
						ORDER BY created DESC';
				}

				if(!array_key_exists('sqlVars', $v))
				{
					$v['sqlVars'] = array(
										'type' => $v['nodeType'],
										'status' => $v['nodeStatus']
									);
				}

				/* Query either the basic or provided SQL query */
				$query = db_query($v['sql'], $v['sqlVars']);
				
				/* Run through the results from the query and load the nodes! */
				while($row = $query->fetchAssoc())
				{
					$nodes[] = node_load($row['nid']);
				}

				/* Attach the nodes to the variable name in the renderable array */
				$renderable['#' . $v['varName']] = $nodes;
			}
		}

		/* If there are any extra options we'll add them to the renderable array also */
		if(count($options))
		{
			foreach($options as $k => $opt)
			{
				$renderable[$k] = $opt;
			}
		}

		/* Finally return the produced renderable array */
		return $renderable;
	}